/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Switch lever
 * Weichenstellhebel
 *
 * Ressources/Quellen
 * [1]: http://www.jagsttalbahn-modelle.de/technik/weichenantrieb/stellhebelmodell.html
 * [2]: http://www.lokodex.de/daten/weian_dxf.zip
 */


// allgemeine Komponenten
include <rcube.scad>
include <rcylinder.scad>
include <support.scad>
include <hexagon.scad>

// spezifische Komponenten
include <axis_h.scad>
include <axis_v.scad>
include <axis_v_lantern_base.scad>
include <base.scad>
include <weight.scad>
include <lever_long.scad>
include <lever_short.scad>
include <lever_y.scad>
include <lantern_base.scad>
include <actuator_h.scad>
include <actuator_v.scad>
include <split_pin.scad>

faktor = 1 / 10; // Maßstab

// Original-Maße in mm

// allgemeine Maße
spaltmass = 1; // Für Beweglichkeit

// Hebelarm allgemeine Maße
arm_lager_dicke = 35; //
arm_lager_durchmesser_aussen = 90; // Durchmesser Lager außen
arm_lager_durchmesser_innen = 40; // Durchmesser Lager innen

arm_l = 580; // Länge ab Mitte Lager
arm_b = 40; // Breite
arm_h = 30; // Dicke

schraube_1_offset_x = 460; // Versatz Befestigungsschraube für Gewicht ab Mitte Lager
schraube_2_offset_x = 170-15; // Versatz Schraube für Verbindung Hebelteile ab Mitte Lager
griff_d = 20; // Durchmesser Loch für Griff
griff_x = 90; // Versatz Griff ab Befestigungsschraube für Gewicht

// Schrauben
d_m16 = 16 + spaltmass; // Durchmesser Loch für Gewinde

// Verdrehsicherungen
verdrehsicherung_d = 10; // Durchmesser

// Typ
/*
 * 0 = Laternensockel und Achse vertikal sind separate Teile, zweiteiliger Hebel
 * 1 = Laternensockel und Achse vertikal sind ein Teil, zweiteiliger Hebel
 * 2 = Laternensockel und Achse vertikal sind separate Teile, einteiliger Hebel
 * 3 = Laternensockel und Achse vertikal sind ein Teil, einteiliger Hebel
 */
type = 0;

// Anzeige der Beschlagteile im zusammengebauten Zustand
beschlagteile = true;
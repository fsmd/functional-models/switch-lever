/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

include <rcube.scad>

// Hebelarm kurzer Teil
module hebel_kurz() {
	// allgemeine Maße
	spaltmass = 1; // Für Beweglichkeit

	// Rechteckprofil
	arm_l = 235; // Länge
	arm_b = arm_b; // Breite
	arm_h = 20; // Höhe
	arm_x = arm_l / 2; // x-Position ggü. Nullpunkt
	arm_z = (arm_lager_dicke - arm_h) / 2 - 1;
	
	// Lager-Gabel
	lager_h = arm_lager_dicke; // Höhe
	lager_d_a = 100; // Durchmesser außen
	lager_d_i = arm_lager_durchmesser_innen + spaltmass; // Durchmesser innen

	// Umfassungsstück
	umfassung_l = 100; // Länge
	umfassung_b = 10; // Breite
	umfassung_h = arm_h + 10; // Höhe
	umfassung_x = arm_l - umfassung_l / 2; // x-Position ggü. Nullpunkt
	umfassung_y = (arm_b + umfassung_b) / 2; // y-Position ggü. Nullpunkt
	umfassung_z = (umfassung_h - arm_h) / 2; // z-Position ggü. Nullpunkt
	
	// Bohrung für Schraube Hebelteile
	loch_schraube_teile_h = arm_h; // Höhe
	loch_schraube_teile_d = d_m16; // Durchmesser
	loch_schraube_teile_x = schraube_2_offset_x; // x-Position ggü. Nullpunkt

	color([0.5, 0, 0])
	union() {
		// Arm
		translate([0, 0, arm_z]) {
			difference() {
				// eigentlicher Arm
				translate([arm_x + (lager_d_i / 4), 0, 0]) cube([arm_l - (lager_d_i / 2), arm_b, arm_h], true);

				// Loch für Schraube
				translate([loch_schraube_teile_x, 0, 0]) cylinder(loch_schraube_teile_h + 1, loch_schraube_teile_d / 2, loch_schraube_teile_d / 2, true);
			}

			// Umfassung
			for ( a = [-1 : 2 : 1] ) {
				translate([umfassung_x, umfassung_y * a, umfassung_z]) cube([umfassung_l, umfassung_b, umfassung_h], true);
			}

			// Ausrundungen
			for ( a = [-1 : 2 : 1] ) translate([arm_l - umfassung_l - umfassung_b / 2, (arm_b + umfassung_b) / 2 * a, 0]) difference() {
				cube([umfassung_b, umfassung_b, arm_h], true);
				translate([- umfassung_b / 2, umfassung_b / 2 * a, 0]) cylinder(arm_h + 1, umfassung_b, umfassung_b, true);
			}
		}

		// Lager-Gabel
		translate([0, 0, spaltmass / 4]) {
			intersection() {
				difference() {
					cylinder(lager_h, lager_d_a / 2, lager_d_a / 2, true);
					cylinder(lager_h, lager_d_i / 2, lager_d_i / 2, true);
				}
				translate([lager_d_a / 4, 0, 0]) cube([lager_d_a / 2, lager_d_a, lager_h - spaltmass / 2], true);
			}

			for ( a = [-1 : 2 : 1] ) {
				translate([-lager_d_a / 4, (lager_d_a + lager_d_i) / 4 * a, 0]) rotate([90 * a, 0, 0]) rotate([90, 0, 0]) rcube([lager_d_a / 2, (lager_d_a - lager_d_i) / 2, lager_h - spaltmass / 2], [5, 0, 0, 5], true);
			}
		}
	}
}
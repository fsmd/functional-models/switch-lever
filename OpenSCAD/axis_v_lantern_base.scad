/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

include <hexagon.scad>
include <trapezoid.scad>

spaltmass = spaltmass;
faktor = faktor;

// Achse vertikal mit Laternensockel
module achse_v_laternensockel() {
    // Flansch
    flansch_h = 17.5;

    // Achse
    achse_l = 382.5 + flansch_h + 2.5; // Länge
    achse_d = 45; // Durchmesser
    achse_z = (flansch_h + 2.5) / 2; // z-Position ggü. Nullpunkt

    // Flansch
	flansch_z = (achse_l - flansch_h) / 2; // z-Position ggü. Nullpunkt

	// Mittenloch
	mittenloch_h = 15 + flansch_h; // Höhe
	mittenloch_d = 30; // Durchmesser
	mittenloch_z = (achse_l + flansch_h - mittenloch_h + 2.5) / 2; // z-Position ggü. Nullpunkt

	// Anlaufring
	ring_h = 20 - spaltmass; // Höhe
	ring_d_a = 70; // Durchmesser außen0
    ring_z = 123.75; // z-Position ggü. Nullpunkt

    // Verdrehsicherung
    verdrehsicherung_z = 33.75; // z-Position ggü. Nullpunkt

    color([1, 0.5, 0])
    difference() {
        union() {
            translate([0, 0, achse_z]) {
                // Achse
                cylinder(achse_l, achse_d / 2, achse_d / 2, true);

                // Flansch
                translate([0, 0, flansch_z]) flansch();
            }

            // Anlaufring
            translate([0, 0, ring_z]) cylinder(ring_h, ring_d_a / 2, ring_d_a / 2, true);
        }

        // Mittenloch
        translate([0, 0, mittenloch_z + 0.5]) cylinder(mittenloch_h + 1, mittenloch_d / 2, mittenloch_d / 2, true);

        // Verdrehsicherung
        translate([0, 0, verdrehsicherung_z]) rotate([0, 90, 135]) cylinder(achse_d, verdrehsicherung_d / 2, verdrehsicherung_d / 2, true);
    }
}

module flansch() {
	// Flansch
	flansch_l = 155; // Länge
	flansch_b = flansch_l; // Breite
	flansch_h = 17.5; // Höhe
	flansch_r = 24; // Radius

	// Bohrungen Schrauben Laternenbefestigung
	loch_schraube_h = flansch_h; // Höhe
	loch_schraube_d = d_m16; // Durchmesser
	loch_schraube_x = 52.5; // x-Position ggü. Nullpunkt
	loch_schraube_y = loch_schraube_x; // y-Position ggü. Nullpunkt


    // Flansch
    difference() {
    	// Flanschplatte
    	rcube([flansch_l, flansch_b, flansch_h], [flansch_r, flansch_r, flansch_r, flansch_r], true);

    	// Bohrungen Schrauben Laternenbefestigung
    	for ( a = [-1 : 2 : 1] ) for ( b = [-1 : 2 : 1] ) {
    		translate([a * loch_schraube_x, b * loch_schraube_y, 0]) cylinder(loch_schraube_h + 1, loch_schraube_d / 2, loch_schraube_d / 2, true);
    	}
    }
}
/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Achse horizontal
module achse_h() {
    // Achse
    achse_l = 330;
    achse_d = arm_lager_durchmesser_innen;

    // Verdrehsicherung 1
    verdrehsicherung_1_x = 57.5 - achse_l / 2; // x-Position ggü. Nullpunkt

    // Verdrehsicherung 2
    verdrehsicherung_2_x = achse_l / 2 - 30; // x-Position ggü. Nullpunkt

    color([1, 0.75, 0])
    difference() {
        // Achse
        rotate([0, 90, 0]) cylinder(achse_l, achse_d / 2, achse_d / 2, true);

        // Verdrehsicherung 1
        rotate([90, 0, 0]) translate([verdrehsicherung_1_x, 0, 0]) cylinder(achse_d, verdrehsicherung_d / 2, verdrehsicherung_d / 2, true);

        // Verdrehsicherung 2
        rotate([90, 0, 0]) translate([verdrehsicherung_2_x, 0, 0]) cylinder(achse_d, verdrehsicherung_d / 2, verdrehsicherung_d / 2, true);
    }
}
/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

include <rcube.scad>
include <trapezoid.scad>

// Y-Arm
module hebel_y() {
	// gemeinsame Maße
	arm_h = 20; // Höhe
	
	// Lager
	lager_h = 35; // Höhe
	lager_d_a = arm_lager_durchmesser_aussen; // Durchmesser außen
	lager_d_i = arm_lager_durchmesser_innen; // Durchmesser innen
	
	// Lager für Gabelkopf
	/*
	 * Famotec (https://www.famotec.de/) Made in Germany
	 *  • M2: 10996 (GCA-20)
	 *  • M3: 10208 (GCA-30)
	 * Vertrieben über Conrad unter Brand Modelcraft als 5er-Pack
	 *  • M2: 206078
     *  • M3: 239186
	 */
	lager_g_d_a = 65; // Durchmesser außen
	lager_g_d_i = 30 + spaltmass; // Durchmesser innen
	lager_g_h =  30 - spaltmass; // Höhe
	lager_g_x = -210; // x-Position ggü. Nullpunkt

	// zulaufender Teil

	// Hilfskreis 1
	arm_t1_k1_h = arm_h; // Höhe
	arm_t1_k1_r = lager_d_a / 2; // Radius

	// Hilfskreis 2
	arm_t1_k2_h = arm_h; // Höhe
	arm_t1_k2_r = lager_g_d_a / 2; // Radius
	arm_t1_k2_x = lager_g_x; // x-Position ggü. Nullpunkt

	// Hilfsquader 1
	arm_t1_q1_l = 70; // Länge
	arm_t1_q1_b = lager_d_a; // Breite
	arm_t1_q1_h = arm_h; // Höhe
	arm_t1_q1_x = arm_t1_q1_l/2; // x-Position ggü. Nullpunkt

	// Y-Ärmchen
	y_l = 120; // Länge
	y_b = 45; // Breite
	y_h = arm_h; // Höhe
	y_x = 8; // x-Position ggü. Nullpunkt
	y_r = 5; // Radius;
	y_winkel = 55; // Winkel ggü. x-Achse

	color([1, 0.5, 0.5])
	difference() {
		union() {
			// zulaufender Teil
			hull() {
				// Hilfsquader 1
				translate([arm_t1_q1_x, 0, 0]) rotate([90, 0, 90]) trapezoid([arm_t1_q1_b, arm_t1_q1_h, arm_t1_q1_b + 10, arm_t1_q1_h, arm_t1_q1_l], true);

				// Hilfskreis 1
				cylinder(arm_t1_k1_h, arm_t1_k1_r, arm_t1_k1_r, true);

				// Hilfskreis 2
				translate([arm_t1_k2_x, 0, 0]) cylinder(arm_t1_k2_h, arm_t1_k2_r, arm_t1_k2_r, true);
			}
			
			// Lager
			cylinder(lager_h - spaltmass, lager_d_a / 2, lager_d_a / 2, true);

			// Lager für Gabelkopf
			translate([lager_g_x, 0 , 0]) cylinder(lager_g_h, lager_g_d_a / 2, lager_g_d_a / 2, true);

			// Y-Ärmchen
			translate([y_x, 0, 0])  for ( a = [-1 : 2 : 1] ) {
				rotate([0, 0, y_winkel * a]) translate([y_l / 2, 0, 0]) rotate([90 * a, 0, 0]) rotate([90, 0, 0]) rcube([y_l, y_b, y_h], [0, y_r * 3, y_r, 0], true);
			}
		}
		// Loch Lager
		cylinder(lager_h, lager_d_i / 2, lager_d_i / 2, true);

		// Loch Lager für Gabelkopf
		translate([lager_g_x, 0, 0]) cylinder(lager_g_h + 1, lager_g_d_i / 2, lager_g_d_i / 2, true);

		// Verdrehsicherung
		rotate([90, 0, 0]) cylinder(lager_d_a + 10, verdrehsicherung_d / 2, verdrehsicherung_d / 2, true);
	}
}
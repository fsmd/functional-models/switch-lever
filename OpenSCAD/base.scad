/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

include <trapezoid.scad>

// Fuß
module fuss() {
	// allgemeine Maße
	spaltmass = 1; // Für Beweglichkeit

	// Sockelplatte
	sockelplatte_l = 180; // Länge
	sockelplatte_b = 350; // Breite
	sockelplatte_h = 25; // Höhe
	sockelplatte_x = sockelplatte_l / 2 + 35; // x-Position ggü. Nullpunkt
	sockelplatte_z = sockelplatte_h / 2; // z-Position ggü. Nullpunkt
	sockelplatte_r = 20; // Radius der Abrundung
	
	// Sockelplatte Fortsatz
	sockelplatte_fo_l = 270 - sockelplatte_l; // Länge
	sockelplatte_fo_b = 110; // Breite
	sockelplatte_fo_h = sockelplatte_h; // Höhe
	sockelplatte_fo_x = sockelplatte_x + (sockelplatte_l + sockelplatte_fo_l) / 2; // x-Position ggü. Nullpunkt
	sockelplatte_fo_z = sockelplatte_fo_h / 2; // z-Position ggü. Nullpunkt
	sockelplatte_fo_r = sockelplatte_fo_b / 2; // Radius der Abrundung

	// Verstärkung Schraubsockel
	schraubsockel_d = 45; // Durchmesser außen
	schraubsockel_h = sockelplatte_h + 5; // Höhe
	schraubsockel_x = 125; // x-Position ggü. Nullpunkt
	schraubsockel_x_o = 60; // x-Position ggü. Nullpunkt Verstärkung Schraubsockel
	schraubsockel_y = 122.5; // y-Position ggü. Nullpunkt
	schraubsockel_z = schraubsockel_h / 2; // z-Position ggü. Nullpunkt
	loch_schraube_d = d_m16; // Durchmesser Loch für Befestigungsschraube

	// Lager für Achse horizontal
	lager_l = 165; // Länge
	lager_d_a = arm_lager_durchmesser_aussen; // Durchmesser außen
	lager_d_i = arm_lager_durchmesser_innen; // Durchmesser innen
	lager_x = lager_l / 2; // x-Position ggü. Nullpunkt
	lager_z = 120; // z-Position ggü. Nullpunkt

	// Lagerbock
	
	// Grundmaße
	lagerbock_l = 120; // Länge am Fußpunkt
	lagerbock_b = schraubsockel_y * 2 - schraubsockel_d; // Breite am Fußpunkt
	lagerbock_h = lager_z - sockelplatte_h; // Höhe am Fußpunkt
	lagerbock_x = lagerbock_l / 2 + 40; // x-Position ggü. Nullpunkt
	lagerbock_z = sockelplatte_h + lagerbock_h / 2; // z-Position ggü. Nullpunkt
	
	// Laternenführung – gemeinsame Maße für oben und unten
	laternenfuehrung_d_i = 45; // Durchmesser innen
	laternenfuehrung_x = 250; // x-Position ggü. Nullpunkt

	// Laternenführung oben
	laternenfuehrung_o_h = 50; // Höhe
	laternenfuehrung_o_d_a = 90; // Durchmesser außen
	laternenfuehrung_o_x = laternenfuehrung_x; // x-Position ggü. Nullpunkt
	laternenfuehrung_o_z = 280;	// z-Position ggü. Nullpunkt

	// Laternenführung unten
	laternenfuehrung_u_r = 85 / 2; // Radius
	laternenfuehrung_u_l = laternenfuehrung_x - (lagerbock_x + lagerbock_l / 2) + laternenfuehrung_u_r;// Länge
	laternenfuehrung_u_b = laternenfuehrung_u_r * 2; // Durchmesser außen
	laternenfuehrung_u_h = 17.5; // Höhe
	laternenfuehrung_u_x = laternenfuehrung_x + (laternenfuehrung_u_b - laternenfuehrung_u_l) / 2; // x-Position ggü. Nullpunkt
	laternenfuehrung_u_z = sockelplatte_h + laternenfuehrung_u_h / 2; // z-Position ggü. Nullpunkt

	// Halterung Laternenführung – gemeinsame Maße
	halter_d = 15; // Dicke
	halter_r = 45; // Radius außen

	// Halterung Laternenführung – flacher Teil
	halter_f_l = laternenfuehrung_x - lagerbock_x - lagerbock_l / 2 + halter_d; // Vorderkante bis Mitte Laternenführung
	halter_f_b = laternenfuehrung_o_d_a; // Breite
	halter_f_h = laternenfuehrung_o_z - laternenfuehrung_o_h / 2 - lager_z + halter_d; // Mitte Lagerachse bis Oberkante

	// Stellwegsbegrenzung

	// Blechteil
	blech_l = arm_lager_dicke * 2 + 35 - 10; // Länge
	blech_b = sockelplatte_b; // Breite
	blech_h = 15; // Höhe
	blech_x = -arm_lager_dicke * 2 + blech_l / 2; // x-Position ggü. Nullpunkt
	blech_z = blech_h / 2; // z-Position ggü. Nullpunkt

	// Ausschnitt
	ausschnitt_l = arm_lager_dicke + 10; // Länge
	ausschnitt_b = blech_b - 65 * 2; // Breite
	ausschnitt_h = blech_h; // Höhe
	ausschnitt_x = blech_x - blech_l / 2 + ausschnitt_l / 2; // x-Position ggü. Nullpunkt
	ausschnitt_z = blech_z; // z-Position ggü. Nullpunkt

	// Unterlage
	unterlage_l = schraubsockel_x + schraubsockel_x_o + schraubsockel_d / 2; // Länge
	unterlage_b = schraubsockel_d; // Breite
	unterlage_h = 20; // Höhe
	unterlage_x = unterlage_l / 2; // x-Position ggü. Nullpunkt
	unterlage_y = schraubsockel_y; // y-Position ggü. Nullpunkt
	unterlage_z = -unterlage_h / 2; // z-Position ggü. Nullpunkt
	
	color([0, 0, 1])
	difference() {
		union() {
			// Sockelplatte
			difference() {
				union() {
					translate([sockelplatte_x, 0, sockelplatte_z]) rcube([sockelplatte_l, sockelplatte_b, sockelplatte_h], [sockelplatte_r, sockelplatte_r, sockelplatte_r, sockelplatte_r], true);

					// Fortsatz
					translate([sockelplatte_fo_x, 0, sockelplatte_fo_z]) rcube([sockelplatte_fo_l, sockelplatte_fo_b, sockelplatte_fo_h], [0, sockelplatte_fo_r, sockelplatte_fo_r, 0], true);

					// Verstärkung Schraubsockel
					hull() translate([schraubsockel_x + schraubsockel_x_o, 0, schraubsockel_z]) {

						translate([-2.5, 0, 0]) cube([schraubsockel_d, lagerbock_b, schraubsockel_h], true);
						translate([4, 0, 0]) cube([schraubsockel_d, laternenfuehrung_u_b, schraubsockel_h], true);

						translate([0, schraubsockel_y, 0]) cylinder(schraubsockel_h, schraubsockel_d / 2, schraubsockel_d / 2, true);
						translate([0, -schraubsockel_y, 0]) cylinder(schraubsockel_h, schraubsockel_d / 2, schraubsockel_d / 2, true);
					}
					
					hull() translate([schraubsockel_x - schraubsockel_x_o, 0, schraubsockel_z]) {
						translate([0, schraubsockel_y, 0]) cylinder(schraubsockel_h, schraubsockel_d / 2, schraubsockel_d / 2, true);
						translate([0, -schraubsockel_y, 0]) cylinder(schraubsockel_h, schraubsockel_d / 2, schraubsockel_d / 2, true);
					}
				}
			}

			// Lager für Achse
			translate([lager_x, 0, lager_z]) rotate([0, 90, 0]) cylinder(lager_l, lager_d_a / 2, lager_d_a / 2, true);

			// Lagerbock
            translate([lagerbock_x, 0, lagerbock_z]) trapezoid([lagerbock_l, lagerbock_b, lagerbock_l, lager_d_a, lagerbock_h], true);

			// Laternenführung oben
			translate([laternenfuehrung_o_x, 0, laternenfuehrung_o_z]) cylinder(laternenfuehrung_o_h, laternenfuehrung_o_d_a / 2, laternenfuehrung_o_d_a / 2, true);
			
			// Laternenführung unten
			translate([laternenfuehrung_u_x, 0, laternenfuehrung_u_z]) rcube([laternenfuehrung_u_l, laternenfuehrung_u_b, laternenfuehrung_u_h], [0, laternenfuehrung_u_r, laternenfuehrung_u_r, 0], true);

			// Halter Laternenführung
			difference() {
				union() {
					// flacher Teil
					translate([laternenfuehrung_x - halter_f_l / 2, 0, lager_z + halter_f_h / 2]) rotate([90, 90, 0]) rcube([halter_f_h, halter_f_l, halter_f_b], [halter_r, 0, 0, 0], true);

					// senkrechter Teil
					translate([0, 0, lager_z]) rotate([90, 0, 0]) linear_extrude(height = halter_d, center = true) import("mount_v.dxf", layer = "Halter_senkrecht");
				}

				// flacher Teil (Aussparung)
				translate([laternenfuehrung_x - (halter_f_l - halter_d) / 2 + 0.5, 0, lager_z + (halter_f_h - halter_d) / 2 - 0.5]) rotate([90, 90, 0]) rcube([halter_f_h - halter_d + 1, halter_f_l - halter_d + 1, halter_f_b + 1], [halter_r - halter_d, 0, 0, 0], true);
			}
				// translate([laternenfuehrung_x - (halter_f_l - halter_d) / 2, 0, lager_z + (halter_f_h - halter_d) / 2]) rotate([90, 90, 0]) rcube([halter_f_h - halter_d, halter_f_l - halter_d, halter_f_b + 100], [halter_r - halter_d, 0, 0, 0], true);
			
			// Stellwegsbegrenzung
			difference() {
				// Blechteil
				translate([blech_x, 0, blech_z]) cube([blech_l, blech_b, blech_h], true);

				// Ausschnitt
				translate([ausschnitt_x - 0.5, 0, ausschnitt_z]) trapezoid([ausschnitt_l + 1, ausschnitt_b + 16 + 8, ausschnitt_l, ausschnitt_b - 8, ausschnitt_h * 2], true);
			}

			// Unterlage
			for ( a = [-1 : 2 : 1] ) {
				translate([unterlage_x, unterlage_y * a, unterlage_z]) cube([unterlage_l, unterlage_b, unterlage_h], true);
			}
		}
		
		// Bohrung Lager
		translate([lager_l / 2, 0, lager_z]) rotate([0, 90, 0]) cylinder(lager_l + 1, lager_d_i / 2 + spaltmass, lager_d_i / 2 + spaltmass, true);

		// Bohrung Laternenführung
		translate([laternenfuehrung_x, 0, (laternenfuehrung_o_z + laternenfuehrung_o_h / 2) / 2]) cylinder(laternenfuehrung_o_z + laternenfuehrung_o_h / 2 + 1, (laternenfuehrung_d_i + spaltmass) / 2, (laternenfuehrung_d_i + spaltmass) / 2, true);

		// Bohrung Verstärkung Schraubsockel
		for ( a = [-1 : 2 : 1] ) {
			for ( b = [-1 : 2 : 1] ) {
				translate([schraubsockel_x + schraubsockel_x_o * b, schraubsockel_y * a, schraubsockel_z + unterlage_z]) cylinder(schraubsockel_h + unterlage_h + 1, loch_schraube_d / 2, loch_schraube_d / 2, true);
			}
		}
	}
}
// Gewicht
module gewicht() {
	// eigentliches Gewicht
	gewicht_h = 105; // Dicke
	gewicht_d = 240; // Durchmesser
	gewicht_r = 8; // Radius der Abrundung

	// Loch für Durchführung Hebelarm
	loch_arm_offset = spaltmass * 2; // Zuschlag zu Hebelarm-Abmessungen, damit dieser eingeschoben werden kann
	loch_arm_h = arm_h + loch_arm_offset; // Hebelarm Dicke
	loch_arm_b = arm_b + loch_arm_offset; // Hebelarm Breite

	// Loch für Handgriff
	loch_griff_h = gewicht_h - 20; // Tiefe
	loch_griff_d = griff_d; // Durchmesser
	loch_griff_x = griff_x; // x-Position ggü. Nullpunkt
	loch_griff_z = (gewicht_h - loch_griff_h) / 2; // z-Position ggü. Nullpunkt

	// Loch für Befestigungsschraube
    loch_schraube_h = gewicht_h - 20; // Tiefe
	loch_schraube_d = d_m16; // Durchmesser
	loch_schraube_z = (gewicht_h - loch_schraube_h) / 2; // z-Position ggü. Nullpunkt

	color([0.5, 0.5, 0.5])
	difference() {
		// eigentliches Gewicht
		rcylinder(gewicht_h, gewicht_d / 2, gewicht_r, true);

		// Loch für Durchführung Hebelarm
		cube([gewicht_d, loch_arm_b, loch_arm_h], true);

		// Loch für Handgriff
		translate([loch_griff_x, 0, loch_griff_z + 0.5]) cylinder(loch_griff_h + 1, loch_griff_d / 2, loch_griff_d / 2, true);

		// Loch für Befestigungsschraube
		translate([0, 0, loch_schraube_z + 0.5]) cylinder(loch_schraube_h + 1, loch_schraube_d / 2, loch_schraube_d / 2, true);
	}
}
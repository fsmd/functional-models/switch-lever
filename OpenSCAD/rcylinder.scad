/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
 // Zylinder mit abgerundeten Kanten
module rcylinder(height, radius, radius_edge, center = false) {
    if(center) {
        translate([0, 0, -height / 2]) 
        rcylinder_offcenter(height, radius, radius_edge);
    }
    else {
        rcylinder_offcenter(height, radius, radius_edge);
    }

	// Ring
	module ring(radius, radius_edge) {
		rotate_extrude() translate([radius - radius_edge, 0, 0]) circle(radius_edge);
	}

	module rcylinder_offcenter(height, radius, radius_edge) {
		translate([0, 0, radius_edge]) hull() {
			ring(radius, radius_edge);
			translate([0, 0, height - radius_edge * 2]) ring(radius, radius_edge);
		}
	}
}

/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Switch lever
 * Weichenstellhebel
 *
 * Darstellung montierter Zustand
 *
 * Ressources/Quellen
 * [1]: http://www.jagsttalbahn-modelle.de/technik/weichenantrieb/stellhebelmodell.html
 * [2]: http://www.lokodex.de/daten/weian_dxf.zip
 */


// spezifische Komponenten
include <switch_lever.scad>

scale(faktor) {
    $fn = 40; // für gröbere Darstellung

    mutter_k = 1.3 * (1 / faktor); // Kopfhöhe
    mutter_s = 3.3 * (1 / faktor); // Schlüsselweite

    // Hebelarm mit Gewicht
    translate([-arm_lager_dicke * 1.5, 0, 120]) rotate([64.5, 0, 0]) rotate([0, 270, 0]) {
        // Gewicht
        rotate([0, 0, 0]) translate([schraube_1_offset_x, 0, 0]) gewicht();
        if(beschlagteile) {
            translate([schraube_1_offset_x, 0, (105 + mutter_k) / 2]) rotate([0, 0, 15]) hexagon(mutter_s, mutter_k, true);
            translate([schraube_1_offset_x + griff_x, 0, 120 / 2 + 16.25]) cylinder(227.5, 25 / 2, 25 / 2, true);
        }

        if(type == 0 || type == 1) {
            // Hebelarm langer Teil
            rotate([0, 0, 0]) translate([0, 0, 0]) hebel_lang();

            // Hebelarm kurzer Teil	
            rotate([0, 0, 0]) translate([0, 0, -arm_lager_dicke]) hebel_kurz();
            if(beschlagteile) for ( a = [-1 : 2 : 1] ) {
                translate([schraube_2_offset_x, 0, 45 * a]) rotate([0, 0, 45 * a]) hexagon(mutter_s, mutter_k, true);
            }
        }
        else if(type == 2 || type == 3) {
            // Hebelarm langer Teil
            rotate([0, 0, 0]) translate([0, 0, 0]) hebel_lang(2);
        }

        // Y-Arm mit Verdrehsicherung
        rotate([0, 0, 32.5]) translate([0, 0, 0]) hebel_y();
        rotate([0, 0, 32.5]) verdrehsicherung([arm_lager_durchmesser_aussen + 6, verdrehsicherung_d]);

        // Mitnehmer vertikal mit Verdrehsicherung
        translate([0, 0, -242.5]) rotate([0, 0, 32.5]) mitnehmer_v();
        translate([0, 0, -242.5]) rotate([0, 0, 32.5]) verdrehsicherung([76, verdrehsicherung_d]);

        // Achse horizontal
        translate([0, 0, -107.5]) rotate([0, 90, 32.5]) achse_h();
    }

    // Fuß
    rotate([0, 0, 0]) translate([0, 0, 0]) fuss();

    // Mitnehmer horizontal mit Verdrehsicherung
    translate([250, 0, 225]) rotate([0, 0, 45]) mitnehmer_h();
    translate([250, 0, 225]) rotate([0, 0, 45]) verdrehsicherung([76, verdrehsicherung_d]);

    if(type == 0 || type == 2) {
        // Achse vertikal
        translate([250, 0, (197.5 + 185) / 2]) achse_v();

        // Laternensockel
        translate([250, 0, 345]) rotate([0, 0, 0]) laternensockel();
        if(beschlagteile) translate([300, 0, 345]) union() {
            rotate([0, 90, 0]) cylinder(20, 16 / 2, 16 / 2, true);
            translate([10, 0, 0]) rotate([20, 0, 0]) rotate([0, 90, 0]) hexagon(mutter_s, mutter_k, true);
        }
    }
    else if(type == 1 || type == 3) {
        // Achse vertikal
        translate([250, 0, (197.5 + 185) / 2]) achse_v_laternensockel();
    }
}
/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Laternensockel
module laternensockel() {
	// allgemeine Maße
	spaltmass = 1; // Für Beweglichkeit

	// Flansch
	flansch_l = 155; // Länge
	flansch_b = flansch_l; // Breite
	flansch_h = 17.5; // Höhe
	flansch_r = 24; // Radius
	flansch_z = (80 + flansch_h) / 2; // z-Position ggü. Nullpunkt

	// Lager
	lager_h = 60; // Höhe
	lager_d_a = 70; // Durchmesser außen
	lager_d_i = 45; // Durchmesser innen
	lager_z = 10; // z-Position ggü. Nullpunkt

	// Mittenloch
	mittenloch_h = flansch_h;
	mittenloch_d = 30;
	mittenloch_z = flansch_z; // z-Position ggü. Nullpunkt

	// Bohrungen Schrauben Laternenbefestigung
	loch_schraube_h = flansch_h; // Höhe
	loch_schraube_d = d_m16; // Durchmesser
	loch_schraube_x = 52.5; // x-Position ggü. Nullpunkt
	loch_schraube_y = loch_schraube_x; // y-Position ggü. Nullpunkt
	loch_schraube_z = flansch_z; // z-Position ggü. Nullpunkt

	// Verdrehsicherung Schraube
	verdrehsicherung_d_a = 40; // Durchmesser außen
	verdrehsicherung_d_i = loch_schraube_d; // Durchmesser innen
	verdrehsicherung_l = 45; // Länge
	verdrehsicherung_x = verdrehsicherung_l / 2; // x-Position ggü. Nullpunkt

	// Sechskant
    /*
     * reines Thema im Funktionsmodell, damit geringere Gefahr des
	 * Verdrehens (Laternensockel ggü. Achse)
     */
	sechskant_l = 38 + spaltmass;
	sechskant_b = sechskant_l;
	sechskant_h = 16;
	sechskant_z = (lager_h - sechskant_h) / 2 + lager_z;

	color([1, 0, 1])
	union() {
		difference() {
			union() {
				// Flansch
				translate([0, 0, flansch_z]) difference() {
					// Flanschplatte
					rcube([flansch_l, flansch_b, flansch_h], [flansch_r, flansch_r, flansch_r, flansch_r], true);

					// Mittenloch
					cylinder(mittenloch_h + 1, mittenloch_d / 2, mittenloch_d / 2, true);

					// Bohrungen Schrauben Laternenbefestigung
					for ( a = [-1 : 2 : 1] ) for ( b = [-1 : 2 : 1] ) {
						translate([a * loch_schraube_x, b * loch_schraube_y, 0]) cylinder(loch_schraube_h + 1, loch_schraube_d / 2, loch_schraube_d / 2, true);
					}
				}

				// Lager
				translate([0, 0, lager_z]) difference() {
					cylinder(lager_h, lager_d_a / 2, lager_d_a / 2, true);
					cylinder(lager_h + 1, lager_d_i / 2, lager_d_i / 2, true);
				}

				// Verdrehsicherung Schraube
				difference() {
					translate([verdrehsicherung_x, 0, 0]) rotate([0, 90,0]) cylinder(verdrehsicherung_l, verdrehsicherung_d_a / 2, verdrehsicherung_d_a / 2, true);
					cylinder(verdrehsicherung_l, lager_d_i / 2, lager_d_i / 2, true);
				}
			}

			// Bohrung Verdrehsicherung Schraube
			translate([verdrehsicherung_x, 0, 0]) rotate([0, 90,0]) cylinder(verdrehsicherung_l + 1, verdrehsicherung_d_i / 2, verdrehsicherung_d_i / 2, true);
		}

		// // Sechskant
		// translate([0, 0, sechskant_z]) difference() {
		// 	cylinder(sechskant_h, lager_d_i / 2, lager_d_i / 2, true);
		// 	hexagon(sechskant_l, sechskant_h + 2, true);
		// }
	}
}
/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Mitnehmer vertikal
module mitnehmer_v() {
	// allgemeine Maße
	spaltmass = 1; // Für Beweglichkeit

	// Lager
	lager_h = 50; // Höhe
	lager_d_a = 70; // Durchmesser außen
	lager_d_i = arm_lager_durchmesser_innen; // Durchmesser innen

	// Mitnehmer
	mitnehmer_l = 125; // Länge
	mitnehmer_d = 20; // Durchmesser
	mitnehmer_x = mitnehmer_l / 2; // x-Position ggü. Nullpunkt
	mitnehmer_z = 8 - (lager_h - mitnehmer_d) / 2; // z-Position ggü. Nullpunkt
	
	color([0, 1, 0])
	difference() {
		union() {
			// Lager
			translate([0, 0, - spaltmass / 4]) cylinder(lager_h - spaltmass / 2, lager_d_a / 2, lager_d_a / 2, true);

			// Mitnehmer
			translate([mitnehmer_x, 0, mitnehmer_z]) rotate([0, 90, 0]) cylinder(mitnehmer_l, mitnehmer_d / 2, mitnehmer_d / 2, true);
		}

		// Bohrung Lager
		cylinder(lager_h + 1, lager_d_i / 2, lager_d_i / 2, true);

		// Verdrehsicherung
		rotate([90, 0, 0]) cylinder(lager_d_a, verdrehsicherung_d / 2, verdrehsicherung_d / 2, true);
	}
}
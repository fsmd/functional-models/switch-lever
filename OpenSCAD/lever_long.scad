/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Type 1 = zweiteiliger Hebel
// Type 2 = einteiliger Hebel

// Hebelarm langer Teil
module hebel_lang(type = 1) {
	// allgemeine Maße
	spaltmass = 1; // Für Beweglichkeit
	uebergang = (type == 1) ? 180 : 154; // Versatz ab Mitte Lager

	// Mutter M1.6 im Funktionsmodell
	/*
	 * reines Thema im Funktionsmodell, weil im Maßstab 
	 * kein funktionsfähiges Gewinde gedruckt werden kann
	 */
	mutter_k = 1.3 * (1 / faktor) + spaltmass; // Kopfhöhe
	mutter_s = 3.3 * (1 / faktor) + spaltmass; // Schlüsselweite

	// Rechteckprofil
	arm_l = arm_l - uebergang; // Länge Hebelarm ab Mitte Lager; - 2 mm, damit Hebelarm nicht aus Gewicht heraus steht
	arm_b = arm_b; // Breite Rechteckprofil
	arm_h = arm_h; // Höhe Rechteckprofil
	arm_x = arm_l / 2 + uebergang; // x-Position ggü. Nullpunkt
	
	// Lager
	lager_h = arm_lager_dicke; // Höhe
	lager_d_a = arm_lager_durchmesser_aussen; // Durchmesser außen
	lager_d_i = arm_lager_durchmesser_innen + spaltmass; // Durchmesser innen
	lager_z = lager_h; // z-Position ggü. Nullpunkt
	
	// Lager Verlängerung
	lager_v_l = uebergang; // Länge
	lager_v_b = arm_b; // Breite
	lager_v_h = 20; // Höhe
	lager_v_x = lager_v_l / 2; // x-Position ggü. Nullpunkt
	lager_v_z = lager_z - (lager_h - lager_v_h) / 2 + 1; // z-Position ggü. Nullpunkt
	
	// Mitnehmer

	// Hilfskreis 1
	mitnehmer_k1_h = arm_lager_dicke + 2; // Höhe
	mitnehmer_k1_r = 5; // Radius
	mitnehmer_k1_x = mitnehmer_k1_r * 2 + 60 + 20; // x-Position ggü. Nullpunkt

	// Hilfsquader 1
	mitnehmer_q1_l = uebergang - 125; // Länge
	mitnehmer_q1_b = arm_b; // Breite
	mitnehmer_q1_h = mitnehmer_k1_h; // Höhe
	mitnehmer_q1_x = uebergang - mitnehmer_q1_l / 2; // x-Position ggü. Nullpunkt

	// Füllstück zu kurzem Armteil
	fuellstueck_l = 235 - uebergang;
	fuellstueck_b = mitnehmer_q1_b;
	fuellstueck_h = (mitnehmer_q1_h - arm_h) / 2;
	fuellstueck_x = 235 - (fuellstueck_l / 2); // x-Position ggü. Nullpunkt;
	fuellstueck_z = -(arm_h + fuellstueck_h) / 2; // z-Position ggü. Nullpunkt;

	// Bohrung für Griff
	loch_griff_h = arm_h; // Höhe
	loch_griff_d = griff_d; // Durchmesser
	loch_griff_x = griff_x + schraube_1_offset_x; // x-Position ggü. Nullpunkt

	// Bohrung für Schraube Gewicht
	loch_schraube_gewicht_h = arm_h; // Höhe
	loch_schraube_gewicht_d = d_m16; // Durchmesser
	loch_schraube_gewicht_x = schraube_1_offset_x; // x-Position ggü. Nullpunkt

	// Bohrung für Schraube Hebelteile
	loch_schraube_teile_h = lager_h * 2; // Höhe
	loch_schraube_teile_d = d_m16; // Durchmesser
	loch_schraube_teile_x = schraube_2_offset_x; // x-Position ggü. Nullpunkt
	loch_schraube_teile_z = arm_h / 2; // z-Position ggü. Nullpunkt

	color([1, 0, 0])
	difference() {
		union() {
			// Mitnehmer
			hull() {
				// Hilfskreis 1
				translate([mitnehmer_k1_x, 0, 0]) cylinder(mitnehmer_k1_h, mitnehmer_k1_r, mitnehmer_k1_r, true);

				// Hilfsquader 1
				translate([mitnehmer_q1_x, 0, 0]) cube([mitnehmer_q1_l, mitnehmer_q1_b, mitnehmer_q1_h], true);
			}
			
			if(type == 1) {
				// Füllstück
				translate([fuellstueck_x, 0, fuellstueck_z]) cube([fuellstueck_l, fuellstueck_b, fuellstueck_h], true);
			}

			// eigentlicher Arm
			translate([arm_x, 0, 0]) cube([arm_l, arm_b, arm_h], true);

			// Verstärkung im Bereich der Mutter M1.6
			translate([loch_schraube_gewicht_x, 0, - (loch_schraube_gewicht_h - mutter_k) / 2 + 3]) cube([30, arm_b + 2, mutter_k + 3], true);

			// 1. Lager mit Verlängerung
			difference() {
				union() {
					// Lager
					translate([0, 0, lager_z - spaltmass / 4]) cylinder(lager_h - spaltmass / 2, lager_d_a / 2, lager_d_a / 2, true);

					// Lager Verlängerung
					translate([lager_v_x, 0, lager_v_z]) cube([lager_v_l, lager_v_b, lager_v_h], true);
				}

				// Bohrung
				translate([0, 0, lager_z]) cylinder(lager_h + 1, lager_d_i / 2, lager_d_i / 2, true);
			}

			if(type == 2) {
				// 2. Lager mit Verlängerung
				difference() {
					union() {
						// Lager
						translate([0, 0, -lager_z + spaltmass / 4]) cylinder(lager_h - spaltmass / 2, lager_d_a / 2, lager_d_a / 2, true);

						// Lager Verlängerung
						translate([lager_v_x, 0, -lager_v_z]) cube([lager_v_l, lager_v_b, lager_v_h], true);
					}

					// Bohrung
					translate([0, 0, -lager_z]) cylinder(lager_h + 1, lager_d_i / 2, lager_d_i / 2, true);
				}				
			}
		}					

		if(type == 1) {
			// Bohrung für Schraube Hebelteile
			translate([loch_schraube_teile_x, 0, loch_schraube_teile_z]) cylinder(loch_schraube_teile_h + 1, loch_schraube_teile_d / 2, loch_schraube_teile_d / 2, true);
		}

		// Bohrung für Schraube Gewicht
		translate([loch_schraube_gewicht_x, 0, 0]) cylinder(loch_schraube_gewicht_h + 1, loch_schraube_gewicht_d / 2, loch_schraube_gewicht_d / 2, true);

		// Bohrung für Griff
		translate([loch_griff_x, 0, 0]) cylinder(loch_griff_h + 1, loch_griff_d / 2, loch_griff_d / 2, true);

        // Aussparung für Mutter M1.6
		translate([loch_schraube_gewicht_x, 0, - (loch_schraube_gewicht_h - mutter_k) / 2 - 0.5]) hexagon(mutter_s, mutter_k + 1, true);
	}
}
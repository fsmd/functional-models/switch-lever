/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Mitnehmer horizontal
module mitnehmer_h() {
	// allgemeine Maße
	spaltmass = 1; // Für Beweglichkeit

	lager_h = 60; // Höhe
	lager_d_a = 70; // Durchmesser außen
	lager_d_i = 45; // Durchmesser innen

	// Mitnehmer
	mitnehmer_l = 82.5; // Länge
	mitnehmer_b = 20; // Breite
	mitnehmer_h = 20; // Höhe
	mitnehmer_x = - mitnehmer_l / 2; // x-Position ggü. Nullpunkt
	mitnehmer_y = (mitnehmer_b - lager_d_a) / 2; // y-Position ggü. Nullpunkt
	mitnehmer_z = (mitnehmer_h - lager_h) / 2; // z-Position ggü. Nullpunkt
	mitnehmer_r = 10; // Radius am Ende

	color([0, 0.5, 0])
	difference() {
		union() {
			// Lager
			translate([0, 0, - spaltmass / 4]) cylinder(lager_h - spaltmass / 2, lager_d_a / 2, lager_d_a / 2, true);
			
			// Mitnehmer
			for ( a = [-1 : 2 : 1] ) {
				translate([mitnehmer_x, a * mitnehmer_y, mitnehmer_z]) rotate([90 * a, 0, 0]) rotate([270, 0, 0]) rcube([mitnehmer_l, mitnehmer_b, mitnehmer_h], [mitnehmer_r, 0, 0, 0], true);
			}
		}

		// Bohrung Lager
		cylinder(lager_h + 1, lager_d_i / 2, lager_d_i / 2, true);

		// Verdrehsicherung
		rotate([90, 0, 0]) cylinder(lager_d_a, verdrehsicherung_d / 2, verdrehsicherung_d / 2, true);
	}
}
/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

include <hexagon.scad>
include <trapezoid.scad>

spaltmass = spaltmass;
faktor = faktor;

// Achse vertikal
module achse_v() {
    achse_l = 382.5 + 1.5;
    achse_d = 45;

	// Mittenloch
	mittenloch_h = 15;
	mittenloch_d = 30;
	mittenloch_z = (achse_l - mittenloch_h) / 2; // z-Position ggü. Nullpunkt

    // Mutter M1.6 im Funktionsmodell
    /*
     * reines Thema im Funktionsmodell, weil im Maßstab 
     * kein funktionsfähiges Gewinde gedruckt werden kann
     */
    mutter_k = 1.3 * (1 / faktor) + spaltmass; // Kopfhöhe
    mutter_s = 3.3 * (1 / faktor) + spaltmass; // Schlüsselweite
    mutter_z = 153.75; // z-Position ggü. Nullpunkt

	// Anlaufring
	ring_h = 20 - spaltmass; // Höhe
	ring_d_a = 70; // Durchmesser außen0
    ring_z = mutter_z - 30; // z-Position ggü. Nullpunkt

    // Verdrehsicherung
    verdrehsicherung_z = 33.75; // z-Position ggü. Nullpunkt

	// Sechskant
    /*
     * reines Thema im Funktionsmodell, damit geringere Gefahr des
	 * Verdrehens (Laternensockel ggü. Achse)
     */
	sechskant_l = 38;
	sechskant_b = sechskant_l;
	sechskant_h = 15;
	sechskant_z = (achse_l - sechskant_h) / 2;

    color([1, 0.5, 0])
    union() {
        difference() {
            translate([0, 0, 0.75]) cylinder(achse_l, achse_d / 2, achse_d / 2, true);

            // Aussparung für Schraube
            translate([achse_d / 4, 0, 153.75]) rotate([0, 90, 0]) cylinder(achse_d / 2, d_m16 / 2, d_m16 / 2, true);

            // Aussparung für Mutter M1.6
            translate([0, achse_d / 4, mutter_z]) cube([mutter_k, achse_d / 2, mutter_s], true);
            translate([0, 0, mutter_z]) rotate([90, 0, 90]) hexagon(mutter_s, mutter_k, true);

            // Mittenloch
            translate([0, 0, mittenloch_z + 0.5]) cylinder(mittenloch_h + 1, mittenloch_d / 2, mittenloch_d / 2, true);

            // Verdrehsicherung
            translate([0, 0, verdrehsicherung_z]) rotate([0, 90, 135]) cylinder(achse_d, verdrehsicherung_d / 2, verdrehsicherung_d / 2, true);

            // // Sechskant
            // translate([0, 0, sechskant_z + 0.5]) difference() {
            //     cylinder(sechskant_h + 1, achse_d / 2 + 1, achse_d / 2 + 1, true);
            //     hexagon(sechskant_l, sechskant_h + 1, true);
            // }
        }

        // Anlaufring
        translate([0, 0, ring_z]) cylinder(ring_h, ring_d_a / 2, ring_d_a / 2, true);
    }
}
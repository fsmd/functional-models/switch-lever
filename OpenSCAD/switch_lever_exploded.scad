/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Switch lever
 * Weichenstellhebel
 *
 * Explosionsdarstellung
 *
 * Ressources/Quellen
 * [1]: http://www.jagsttalbahn-modelle.de/technik/weichenantrieb/stellhebelmodell.html
 * [2]: http://www.lokodex.de/daten/weian_dxf.zip
 */


// spezifische Komponenten
include <switch_lever.scad>

scale(faktor) {
    $fn = 40; // für gröbere Darstellung

    // Gewicht
    translate([-200, 0, 900]) rotate([0, 270, 0]) gewicht();

    if(type == 0 || type == 1) {
        // Hebelarm langer Teil
        translate([-200, 0, 120]) rotate([0, 270, 0]) hebel_lang();

        // Hebelarm kurzer Teil	
        translate([0, 0, 120]) rotate([0, 270, 0]) hebel_kurz();
    }

    if(type == 2 || type == 3) {
        // Hebelarm langer Teil
        translate([-200, 0, 120]) rotate([0, 270, 0]) hebel_lang(2);
    }

    // Y-Arm mit Verdrehsicherung
    hebel_y_offset_x = (type == 0 || type == 1) ? -100 : -70;
    translate([hebel_y_offset_x, 0, 120]) rotate([0, 270, 0]) hebel_y();
    translate([hebel_y_offset_x, -160, 120]) verdrehsicherung([arm_lager_durchmesser_aussen + 6, verdrehsicherung_d]);

    // Fuß
    rotate([0, 0, 0]) translate([100, 0, 0]) fuss();

    // Mitnehmer vertikal mit Verdrehsicherung
    translate([450, 0, 120]) rotate([0, 270, 0]) mitnehmer_v();
    translate([450, -160, 120]) verdrehsicherung([76, verdrehsicherung_d]);

    // Achse horizontal
    translate([690, 0, 120]) achse_h();

    // Mitnehmer horizontal mit Verdrehsicherung
    translate([350, 0, 400]) rotate([0, 0, 0]) mitnehmer_h();
    translate([350, -160, 400]) verdrehsicherung([76, verdrehsicherung_d]);

    if(type == 0 || type == 2) {
        // Achse vertikal
        translate([350, 0, 690]) rotate([0, 0, -45]) achse_v();

        // Laternensockel
        translate([350, 0, 980]) rotate([0, 0, -45]) laternensockel();
    }
    else if(type == 1 || type == 3) {
        // Achse vertikal
        translate([350, 0, 690]) rotate([0, 0, -45]) achse_v_laternensockel();
    }
}
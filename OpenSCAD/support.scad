/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

module support(length, faktor = 1) {
    radius_middle = 0.75 / faktor; // radius middle section
    radius_end = 0.3 / faktor; // radius at ends of support

    // length = length * faktor;
    length_end = 1 / faktor; // length of ends
    length_middle = (length - length_end * 2);

    union() {
        translate([0, 0, -(length_middle + length_end) / 2]) cylinder(length_end, radius_end, radius_middle, true);
        translate([0, 0, (length_middle + length_end) / 2]) cylinder(length_end, radius_middle, radius_end, true);
        cylinder(length_middle, radius_middle, radius_middle, true);
        cylinder(length + 1 / faktor, radius_end, radius_end, true);
    }
}
/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// Quader mit horizontal abgerundeten Kanten
module rcube(size, radius, center = false) {
	// source: https://blog.prusaprinters.org/parametric-design-in-openscad/

    if(center) {
        translate([-size[0] / 2, -size[1] / 2, -size[2] / 2])
        rcube_offcenter();
    }
    else {
        rcube_offcenter();
    }

    module rcube_offcenter() {
        if(len(radius) == undef) {
            // The same radius on all corners
            rcube(size, [radius, radius, radius, radius]);
        } 
        else if(len(radius) == 2) {
            // Different radii on top and bottom
            rcube(size, [radius[0], radius[0], radius[1], radius[1]]);
        }
        else if(len(radius) == 4) {
            // Different radius on different corners
            hull() {
                // BL
                if(radius[0] == 0) cube([1, 1, size[2]]);
                else translate([radius[0], radius[0]]) cylinder(r = radius[0], h = size[2]);
                // BR
                if(radius[1] == 0) translate([size[0] - 1, 0]) cube([1, 1, size[2]]);
                else translate([size[0] - radius[1], radius[1]]) cylinder(r = radius[1], h = size[2]);
                // TR
                if(radius[2] == 0) translate([size[0] - 1, size[1] - 1])cube([1, 1, size[2]]);
                else translate([size[0] - radius[2], size[1] - radius[2]]) cylinder(r = radius[2], h = size[2]);
                // TL
                if(radius[3] == 0) translate([0, size[1] - 1]) cube([1, 1, size[2]]);
                else translate([radius[3], size[1] - radius[3]]) cylinder(r = radius[3], h = size[2]);
            }		
        } 
        else {
            echo("ERROR: Incorrect length of 'radius' parameter. Expecting integer or vector with length 2 or 4.");
        }    
    }
}
